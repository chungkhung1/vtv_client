//

//

#import <UIKit/UIKit.h>

@interface HotCollectionViewVideoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVideo;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoName;

@end

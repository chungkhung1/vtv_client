//
//  CollectionViewCellVideos.h
//  KisimeTV
//
//  Created by Luong The Dung on 11/18/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol HotCollectionViewDelegate<NSObject>
//@optional
//- (void)didSelectHotVideo:(Video*)videoSelected;
//@end

@interface CollectionViewCellVideos : UICollectionViewCell<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewVideos;
@property (strong, nonatomic) NSArray* arrVideo;
//@property (nonatomic, weak) id <HotCollectionViewDelegate> delegate;

@end

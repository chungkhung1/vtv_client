//
//  CollectionViewCellVideos.m
//  KisimeTV
//
//  Created by Luong The Dung on 11/18/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "CollectionViewCellVideos.h"
#import "HotCollectionViewVideoCell.h"

@implementation CollectionViewCellVideos

- (void)awakeFromNib {
    [super awakeFromNib];
    _collectionViewVideos.delegate = self;
    _collectionViewVideos.dataSource = self;
    [_collectionViewVideos registerNib:[UINib nibWithNibName:@"HotCollectionViewVideoCell" bundle:nil] forCellWithReuseIdentifier:@"HotCollectionViewVideoCell"];
    
    // Initialization code
}

#pragma mark => collectionView delegate and datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrVideo.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
   
    return CGSizeMake((_collectionViewVideos.frame.size.width-30)/2, _collectionViewVideos.frame.size.height);
  
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
static NSString* cellIdentifier = @"HotCollectionViewVideoCell";
    HotCollectionViewVideoCell *cell = (HotCollectionViewVideoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
//    Video *videoTemp = [_arrVideo objectAtIndex:indexPath.row];
//    [cell.imgVideo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", BASE_DOMAIN, videoTemp.vImage]] placeholderImage:IMAGE_HOLDER];
//    cell.lblVideoName.text = videoTemp.vName;
    return cell;
}

//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    [_delegate didSelectHotVideo:_arrVideo[indexPath.row]];
//}

@end

//
//  CollectionViewCell.h
//  KisimeTV
//
//  Created by Luong The Dung on 11/18/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>
#

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;
//- (void)setupDataWithCategoryObj:(Category*)cateObj;
@end

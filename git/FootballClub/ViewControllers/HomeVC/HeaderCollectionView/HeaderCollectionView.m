//

//

#import "HeaderCollectionView.h"

@implementation HeaderCollectionView{
    BOOL isPlaying;
    BOOL isInView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    isPlaying = NO;
    isInView = YES;
    _viewYTPlayer.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLiveVideo) name:RELOAD_LIVE_VIDEO object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLiveVideo) name:STOP_LIVE_VIDEO object:nil];
    // Initialization code
//    if (gLiveVideo.link.length) {
//        [self getLiveVideo];
//    }
    
  
}

- (void)getLiveVideo{
    if (isPlaying || !isInView) {
        return;
    }
//    [NetworkManager getIdLinkLiveVideoFromId:gLiveVideo.link successHandler:^(NSString *idLinkLiveReturn) {
        if (self) {
            NSDictionary *playerVars = @{
                                         @"controls" : @"1",
                                         @"playsinline" : @"1",
                                         @"autohide" : @"1",
                                         @"showinfo" : @"0",
                                         @"autoplay" : @"1",
                                         @"fs" : @"1",
                                         @"rel" : @"0",
                                         @"loop" : @"0",
                                         @"enablejsapi" : @"1",
                                         @"modestbranding" : @"1",
                                         };
//            [self.viewYTPlayer loadVideoByURL:idLinkLiveReturn startSeconds:0 suggestedQuality:kYTPlaybackQualityAuto];
          //  [self.viewYTPlayer loadWithVideoId:gLiveVideo.link playerVars:playerVars];
        }
//    } failure:^(NSString *err) {
//        
//    }];
}

- (void)stopLiveVideo{
    
    [_viewYTPlayer stopVideo];
    isPlaying = NO;
    isInView = NO;
}

#pragma mark => PlayerView Delegate
- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    if (!isPlaying && isInView) {
        isPlaying = YES;
        [playerView playVideo];
    }
}

- (UIView *)playerViewPreferredInitialLoadingView:(YTPlayerView *)playerView{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"imgHolder"]];
    
    return imgView;
}




@end

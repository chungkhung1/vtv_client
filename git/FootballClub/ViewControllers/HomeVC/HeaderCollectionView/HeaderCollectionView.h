//

//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"
#define RELOAD_LIVE_VIDEO @"RELOAD_LIVE_VIDEO"
#define STOP_LIVE_VIDEO     @"STOP_LIVE_VIDEO"

@interface HeaderCollectionView : UICollectionReusableView<YTPlayerViewDelegate>
@property (weak, nonatomic) IBOutlet YTPlayerView *viewYTPlayer;

@end

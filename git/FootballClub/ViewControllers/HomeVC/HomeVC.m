//
//  HomeVC.m


#import "HomeVC.h"
#import "CollectionViewCellVideos.h"
#import "CollectionViewCell.h"
#import "HeaderCollectionView.h"
#import "HeaderSectionCollectionView.h"
#import "InfiniteSlideShowDatasource.h"
#import "InfiniteSlideShowDelegate.h"
#import "InfiniteSlideShow.h"
#import "CustomPageControl.h"
#import "LiveChanelVC.h"
#import "LiveChanelTbCell.h"
#import "PlayDetailVideoVC.h"



@interface HomeVC ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, InfiniteSlideShowDelegate, InfiniteSlideShowDatasource,UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *arrMulVideos;
}
@property (weak, nonatomic) IBOutlet UIView *btnVirtualView;
@property (weak, nonatomic) IBOutlet UITableView *btnChannelLive;

@end

@implementation HomeVC{
    NSMutableArray *arrMultVideos;
    InfiniteSlideShow *slideShow;
    InfiniteSlideShow *slideShowWithCustomControl;
    CustomPageControl *pageControl;
    NSArray *dataArray;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnChannelLive.delegate = self ;
    self.btnChannelLive.dataSource = self ;
    self.btnChannelLive.estimatedRowHeight = 44;
    self.btnChannelLive.rowHeight = UITableViewAutomaticDimension;
    [self configViewController];
    [self getDataFromServer];
    [self setupSlider] ;
    [UIViewController attemptRotationToDeviceOrientation];
    
    // Do any additional setup after loading the view.
}
-(void)setupSlider
{
    dataArray = @[@"http://quanbongda.net/public/noucamp.jpg",
                  @"http://quanbongda.net/public/elclassical.jpg",
               ];
    
    //    // Default Slideshow
    //    NSLog(@"size width : %ld" , self.view.frame.size.width );
    //    slideShow = [[InfiniteSlideShow alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 10)];
    //    slideShow.dataSource = self;
    //    slideShow.delegate = self;
    //    [slideShow setUpViewWithTimerDuration:nil
    //                        animationDuration:nil
    //                        customPageControl:nil];
    //[self.view addSubview:slideShow];
    
    
    // Slideshow with custom animation / timer and custom page control
    CGRect myFrame = [_btnVirtualView frame];
    
    slideShowWithCustomControl = [[InfiniteSlideShow alloc] initWithFrame:myFrame];
    slideShowWithCustomControl.dataSource = self;
    slideShowWithCustomControl.delegate = self;
    
    pageControl = [[CustomPageControl alloc] init];
    pageControl.hidesForSinglePage = YES;
    [pageControl setNumberOfPages:[dataArray count]] ;
    [pageControl setCurrentPage:0];
    [pageControl setOnImage:[UIImage imageNamed:@"dot_on"]];
    [pageControl setOffImage:[UIImage imageNamed:@"dot_off"]];
    [pageControl setIndicatorDiameter: 10.0f];
    [pageControl setIndicatorSpace:7.0f];
    [slideShowWithCustomControl setBackgroundColor:[UIColor redColor]];
    
    [slideShowWithCustomControl setUpViewWithTimerDuration:[NSNumber numberWithFloat:5.0]
                                         animationDuration:[NSNumber numberWithFloat:0.2]
                                         customPageControl:pageControl];
    
    [self.view addSubview:slideShowWithCustomControl];
    // Setting up TimeInterval
    [NSTimer scheduledTimerWithTimeInterval:10.0
                                     target:self
                                   selector:@selector(mytimeout)
                                   userInfo:nil
                                    repeats:NO];
}
- (void)mytimeout {
    NSLog(@"timeout");
    dataArray = @[@"http://quanbongda.net/public/noucamp.jpg",
                  @"http://quanbongda.net/public/elclassical.jpg",
                  ];
    [slideShow reload];
    [slideShowWithCustomControl reload];
}


- (void)configViewController{
    [_btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    arrMultVideos = [[NSMutableArray alloc] init];
    [_collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCellVideos" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCellVideos"];
    [_collectionView registerClass:[HeaderCollectionView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderCollectionView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"HeaderCollectionView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderCollectionView"];
    
    [_collectionView registerClass:[HeaderSectionCollectionView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderSectionCollectionView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"HeaderSectionCollectionView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderSectionCollectionView"];
}

- (void)getDataFromServer{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager getListVideoInLiveChanelWithSuccessHandler:^(NSArray *arrVideosReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        arrMulVideos = arrVideosReturn.mutableCopy;
        [self.btnChannelLive reloadData];
    } failure:^(NSString *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
    }];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect rightFrame = [_btnVirtualView frame];
    slideShowWithCustomControl.frame = rightFrame;
}

#pragma mark =>TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMulVideos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"LiveChanelTbCell";
    LiveChanelTbCell *cell = (LiveChanelTbCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Video *videoTemp = [arrMulVideos objectAtIndex:indexPath.row];
    [cell setupDataCellWithVideoObj:videoTemp];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str_token  = [[NSUserDefaults standardUserDefaults]valueForKey:@"token"] ;
    NSInteger leng = [str_token length] ;
    if (!gUser && leng == 0) {
        [Util showAlert:@"Vui lòng đăng nhập để xem video" withTitle:APP_NAME atController:self];
        return;
        
    }
    Video *tempVideo = [arrMulVideos objectAtIndex:indexPath.row];
    [self getVideoLinkofVideoObj:tempVideo];
    
}

- (void)getVideoLinkofVideoObj:(Video*)videoObj{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager getLinkVideoWithVideoId:videoObj.videoId successHandler:^(Video *videoObjReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self gotoPlayVideo:videoObjReturn];
        
    } failureHandler:^(NSString *err, NSString *status) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([status isEqualToString:@"0"]) {
            [self askUserBuyVideo:videoObj];
        }else{
            [self.view makeToast:err duration:2.0 position:CSToastPositionBottom];
        }
    }];
}

- (void)askUserBuyVideo:(Video*)videoObj{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Mua video" message:@"Xác nhận mua video để tiếp tục xem"preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *btOK = [UIAlertAction actionWithTitle:@"Đồng ý" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self buyVideoWithVideoObj:videoObj];
    }];
    
    UIAlertAction *btCancel = [UIAlertAction actionWithTitle:@"Huỷ" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:btOK];
    [alert addAction:btCancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)buyVideoWithVideoObj:(Video*)videoObj{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager buyVideoWithVideoId:videoObj.videoId successHandler:^(Video *videoObjectReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self gotoPlayVideo:videoObjectReturn];
    } failureHandler:^(NSString *err, NSString *status) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionBottom];
    }];
    
}

- (void)gotoPlayVideo:(Video*)videoObj{
    PlayDetailVideoVC *playVideoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayDetailVideoVC"];
    playVideoVC.videoObj = videoObj;
    [self.navigationController pushViewController:playVideoVC animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:STOP_LIVE_VIDEO object:nil];
}
#pragma mark =>CollectionView Datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 3;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 1) {
        if (arrMultVideos.count > 0) {
            return 1;
        }else{
            return 0;
        }
    }else if (section == 2){
        //  return gArrCate.count;
        return 0;
    }else{
        return 0;
    };
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        CollectionViewCellVideos *cell = (CollectionViewCellVideos*)[_collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCellVideos" forIndexPath:indexPath];
        cell.arrVideo = arrMultVideos;
        //  cell.delegate = self;
        return cell;
    }else{
        CollectionViewCell *cell = (CollectionViewCell*)[_collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
        //        Category *cateTemp = gArrCate[indexPath.row];
        //        [cell setupDataWithCategoryObj:cateTemp];
        return cell;
    }
}


#pragma mark => CollectionView Delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return CGSizeMake(_collectionView.frame.size.width, (_collectionView.frame.size.width -30)/2/16*9);
    }else{
        return CGSizeMake((_collectionView.frame.size.width - 20)/2, (_collectionView.frame.size.width -20)/2/16*9);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 1 || section == 2) {
        return CGSizeMake(collectionView.frame.size.width, 50);
    }else{
        return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.width/16*9);
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)theCollectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)theIndexPath
{
    HeaderSectionCollectionView *header = nil;
    if ([kind isEqualToString: UICollectionElementKindSectionHeader]) {
        
        if (theIndexPath.section == 0) {
            HeaderCollectionView *headerView = [theCollectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderCollectionView" forIndexPath:theIndexPath];
            return headerView;
        }else{
            HeaderSectionCollectionView *headerView = [theCollectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderSectionCollectionView" forIndexPath:theIndexPath];
            if (theIndexPath.section == 1) {
                headerView.lblHeader.text = @"Nổi bật";
                
            }else{
                headerView.lblHeader.text = @"Chủ đề";
            }
            return headerView;
        }
    }
    
    return header;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        //        Category* cateTemp = gArrCate[indexPath.row];
        //        CategoryDetailVC *cateVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryDetailVC"];
        //        cateVC.cateObj = cateTemp;
        
        //    [self.revealViewController pushFrontViewController:cateVC animated:YES];
    }
}

#pragma mark => Delegate CollectionVideo
//- (void)didSelectHotVideo:(Video *)videoSelected{
//    PlayDetailVideoVC* detailVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayDetailVideoVC"];
//    detailVideo.videoObj = videoSelected;
//    [self.navigationController pushViewController:detailVideo animated:YES];
//}



- (UIStatusBarStyle)preferredStatusBarStyle{
    return kStatusBarStyle;
}

- (void)didClickSlideShowItem:(id)sender
{
    NSLog(@"Did click slide show item");
}

#pragma mark InfiniteSlideshow Datasource
- (NSArray *)loadSlideShowItems
{
    NSLog(@"Loading slide show items.");
    return [dataArray copy];
}
@end

//
//  BetViewController.m
//  FootballClub
//
//  Created by LuongTheDung on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "BetViewController.h"
#import "BetTableViewCell.h"
#import "BetDetailViewController.h"
#import "MatchObj.h"

@interface BetViewController () {
    NSMutableArray *arrMulVideos;
    NSDictionary *outputDic ;
    NSMutableArray *lstQuestion ;
    
    int numberOfMatch ;
}

- (IBAction)btnMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UILabel *lbl_labelBet;
@property (weak, nonatomic) IBOutlet UITableView *btn_tableView;
@property (strong, nonatomic) MatchObj *match;

@end

@implementation BetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrMulVideos = [[NSMutableArray alloc] init];
    [self configViewController];
    [self getDataFromSever];
    // Do any additional setup after loading the view.
}
- (void)configViewController{
    [_btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    self.btn_tableView.tableFooterView = [UIView new];
    self.btn_tableView.dataSource = self;
    self.btn_tableView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)getDataFromSever{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *str_token = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"] ;
    
    [ModelManager getListQuestion:str_token successHandler:^(NSDictionary *userObjReturn) {
        //userObjReturn = outputDic ;
       arrMulVideos  = [userObjReturn valueForKey:@"data"] ;
//        lstQuestion = [arrMulVideos valueForKey:@"questions"] ;
        numberOfMatch = arrMulVideos.count;
//        NSNumber  *matchID = [arrMulVideos valueForKey:@"matchID"] ;
//        numberOfMatch = [matchID intValue] ;
       // outputDic = dataDic ;
        
//        self.match = [MatchObj instantiateWithDictionary:userObjReturn[@"data"]];
        [self.btn_tableView reloadData];
        
        
    } failureHandler:^(NSString *strErrReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.view makeToast:strErrReturn duration:2.0 position:CSToastPositionCenter];
    }] ;
    
}
#pragma mark =>TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    
 //   return 1 ;
//    NSLog(@"Value of count : %ld", arrMulVideos.count) ;
    return numberOfMatch;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"BetTableViewCell";
    BetTableViewCell *cell = (BetTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
       NSMutableArray *listQuestions = [arrMulVideos[indexPath.row] valueForKey:@"questions"] ;
    BOOL isBet ;
    if ([listQuestions isKindOfClass:[NSNull class]]) {
        isBet = FALSE ;
    }
    else
    {
        isBet = TRUE ;
    }
    MatchObj *match = [MatchObj instantiateWithDictionary:arrMulVideos[indexPath.row]];
    
   // NSDictionary *dict = [arrMulVideos[indexPath.row] valueForKey:@"questions"];
    [cell configWithMatch:match colorStatuc:isBet] ;
    
//    [cell configWithMatch:match, true];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
  
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BetDetailViewController *playVideoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BetDetailViewController"];
    //playVideoVC.lstQuestion = videoObj;
//    [arrMulVideos valueForKey:@"questions"]
    NSMutableArray *listQuestions = [arrMulVideos[indexPath.row] valueForKey:@"questions"] ;
    
    NSNumber *matchID = [arrMulVideos [indexPath.row] valueForKey:@"matchID"];
    
    playVideoVC.matchID = [matchID integerValue] ;
    playVideoVC.lstQuestion = listQuestions;
    
    if (![listQuestions isKindOfClass:[NSNull class]]) {
        [self.navigationController pushViewController:playVideoVC animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Không có câu hỏi cho trận đấu" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Thoát", nil] ;
        [alert show];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end

//

//

#import "PlayDetailVideoVC.h"



@interface PlayDetailVideoVC () <YTPlayerViewDelegate>

@end

@implementation PlayDetailVideoVC{
NSArray *arrRelatedVideos;
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return kStatusBarStyle;
}
- (BOOL) shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configViewController];

    // Do any additional setup after loading the view.
}

- (void)configViewController{
    _viewYTPlayerVideo.delegate = self;
    _lblTitle.text = _videoObj.name;
    NSArray *strArr = [_videoObj.linkVideo componentsSeparatedByString:@"?v="];
    NSDictionary *playerVars = @{
                                 @"controls" : @1,
                                 @"playsinline" : @1,
                                 @"autohide" : @1,
                                 @"showinfo" : @0,
                                 @"modestbranding" : @1
                                 };
    
    [self.viewYTPlayerVideo loadWithVideoId:[strArr lastObject] playerVars:playerVars];
}



- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark =>> YTPlayerView delegate

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    if (_viewYTPlayerVideo == playerView) {
        [_viewYTPlayerVideo playVideo];
    }
}




@end

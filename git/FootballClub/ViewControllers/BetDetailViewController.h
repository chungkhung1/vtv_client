//
//  BetDetailViewController.h
//  FootballClub
//
//  Created by LuongTheDung on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BetDetailViewController : UIViewController
@property (strong, nonatomic) NSMutableArray* lstQuestion;
@property NSInteger matchID ;
@end

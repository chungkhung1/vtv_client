//
//  BetDetailViewController.m
//  FootballClub
//
//  Created by LuongTheDung on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "BetDetailViewController.h"
#import "BetDetailViewCell.h"
#import "HDAlertView.h"
#import "ModelManager.h"
#import "SCLAlertView.h"

@interface BetDetailViewController ()<UITableViewDelegate , UITableViewDataSource> {
    int numberOfRow;
    NSIndexPath *numberOfRowClicked ;
}

@property (weak, nonatomic) IBOutlet UITableView *btnTableQuestion;

@end

@implementation BetDetailViewController

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_lstQuestion count] ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_lstQuestion objectAtIndex:indexPath.row] ;
    static NSString* cellIdentifier = @"BetDetailViewCell";
    
    BetDetailViewCell *cell = (BetDetailViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSString *question = [dict valueForKey:@"question"] ;
    NSDictionary *dictBet = [dict valueForKey:@"bet"] ;
    NSNumber*  tbetStatus  = [dictBet valueForKey:@"status"] ;
 
        
  
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    
    label.hidden = true ;
   
    if ([tbetStatus isKindOfClass:[NSNull class]])
{
        UIColor *color = [UIColor colorWithRed:0.5f green:0.8f blue:0.8f alpha:1.0f];
        [cell.color_view setBackgroundColor:color] ;
       // cell.btn_DatCuoc.titleLabel.text = @"Đã Đặt Cược";
        label.text = @"0" ;
     
       

    }
    
       cell.noidungcauhoi.text = question  ;
    cell.noidungcauhoi.editable = NO ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
  //  [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    return cell ;
    
}

- (void) updateData : (NSString*)str_question
{
   
    NSString *strTOken = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"];
    [ModelManager getListOneQuestion:strTOken questionID:str_question successHandler:^(NSDictionary *userObjReturn) {
        //code
    } failureHandler:^(NSString *strErrReturn) {
      //  <#code#>
    }] ;
}
-(void) CancelBet :(NSString *)questionID : (NSString*) anwersID
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Huỷ Cược" message:@"Bạn có muốn huỷ cược"preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *btOK = [UIAlertAction actionWithTitle:@"Đồng ý" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *api_Path = @"cancelBet" ;
        NSDictionary *param;
        param = @{@"questionID": questionID ,
                  @"anwerID": anwersID
                  } ;
        
        
       // NSString *api_Path = @"auth/resetPassword" ;
        
        [ModelManager sendPOSTRequestViaFormDataWithAPIPath:api_Path Param:param successHandler:^(id idSuccess) {
            
            NSString *str_questionID =  [NSString stringWithFormat:@"%ld" ,(long)_matchID] ;
            [self updateData:str_questionID];
            
        } failureHandler:^(NSString *err) {
            
            UIAlertView *infor = [[UIAlertView alloc ] initWithTitle:@"Thông báo" message:err delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil] ;
            [infor show ];
            
        }];
        
    }];
    
    UIAlertAction *btCancel = [UIAlertAction actionWithTitle:@"Huỷ" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:btOK];
    [alert addAction:btCancel];
    [self presentViewController:alert animated:YES completion:nil];

    }



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BetDetailViewCell *betCell = (BetDetailViewCell*) [tableView cellForRowAtIndexPath:indexPath] ;
    UILabel *labelCheck = [betCell viewWithTag:1] ;
    NSString *valueCheck = labelCheck.text ;
    numberOfRowClicked = indexPath ;
    
    
    if ([valueCheck integerValue] == 0)
    {
              
        
        [self CancelBet :@"1" : @"1"];
    }
    else
    {
        NSDictionary *dict = [_lstQuestion objectAtIndex:indexPath.row] ;
        NSNumber *questionID = [dict valueForKey:@"questionID"];
        NSString *question = [dict valueForKey:@"question"] ;
        NSArray *array = [dict valueForKey:@"answers"] ;
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        SCLTextView *textField = [alert addTextField:@"Số tiền cược(Ball)"];
        textField.keyboardType = UIKeyboardTypeNumberPad;
        
        alert.hideAnimationType = SCLAlertViewHideAnimationSimplyDisappear;
        alert.customViewColor = COLOR_BG_NAVIBAR;
        alert.shouldDismissOnTapOutside = YES;
        for (int i = 0 ; i< [array count] ; i++)
        {
            NSDictionary *dic = [array objectAtIndex:i];
            NSString *answer = [dic valueForKey:@"answer"];
            NSNumber *anwersID = [dic valueForKey:@"answerID"];
            [alert addButton:answer :[anwersID integerValue] actionBlock:^(void)
             {
                 if (textField.text.length > 0) {
                     [self postAnswerWithQuestion:questionID withAnswer:anwersID withBet:textField.text];
                 } else {
                     
                 }
                 
                 NSLog(@"anwerID = %ld" , (long)anwersID) ;
                 NSLog(@"Text value: %@ ", textField.text);
                 NSLog(@"dic = %@" , dic);
                 
             }] ;
        }
        
        
        
        if([array count ] == 4)
        {
            [alert addButton:@"Show Name" actionBlock:^(void) {
                NSLog(@"Text value: %@", textField.text);
            }];
            [alert addButton:@"Show Name2" actionBlock:^(void) {
                NSLog(@"Text value: %@", textField.text);
            }];
            [alert addButton:@"Show Name3" actionBlock:^(void) {
                NSLog(@"Text value: %@", textField.text);
            }];
            
        }
        
        // [alert showEdit:self title:kInfoTitle subTitle:kSubtitle closeButtonTitle:kButtonTitle duration:0.0f];
        [alert showEdit:self title:@"Dự đoán" subTitle:question closeButtonTitle:@"Done" duration:0.0f];
    }
    
    
   
    
}

- (void) postAnswerWithQuestion:(NSNumber *)questionID withAnswer:(NSNumber *)answerID withBet:(NSString *)betValue {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *str_token = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"];
    
    [ModelManager betMatchWithUserToken:str_token questionID:questionID answerID:answerID betValue:betValue successHandler:^(NSDictionary *userObjReturn) {
        
        NSDictionary *ano_money = [userObjReturn valueForKey:@"data"] ;
        NSNumber *money = [ano_money valueForKey:@"userMoney"] ;
        [[NSUserDefaults standardUserDefaults]setObject:money forKey:@"money"];
        [[NSUserDefaults standardUserDefaults]synchronize ] ;
        [self touchUpInside_btnBack:nil];
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"" message:@"Đặt cược thành công" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        [ alerView show];
    } failureHandler:^(NSString *strErrReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:strErrReturn duration:2.0 position:CSToastPositionCenter];
    }];
}


#pragma mark - life cycle of view
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
    
}
- (void) reloadData
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public methods
- (void) initView {
    self.btnTableQuestion.delegate = self ;
    self.btnTableQuestion.dataSource = self ;
}

#pragma mark - Private methods

#pragma mark - IBAction
- (IBAction)touchUpInside_btn:(id)sender {
    
}

- (IBAction)touchUpInside_btnBack:(id)sender {
    

    
    [self.navigationController popViewControllerAnimated:true];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

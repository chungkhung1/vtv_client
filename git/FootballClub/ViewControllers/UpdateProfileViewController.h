//
//  UpdateProfileViewController.h
//  FootballClub
//
//  Created by LuongTheDung on 12/27/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateProfileViewController : UIViewController



@property (weak, nonatomic) IBOutlet UITextField *txt_ten;
@property (weak, nonatomic) IBOutlet UITextField *txt_hodem;

@property (weak, nonatomic) IBOutlet UITextField *txt_email;

@property (weak, nonatomic) IBOutlet UITextField *txtPhone;

- (IBAction)btnUpdate:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
- (IBAction)btnAction:(id)sender;

@end



//
// 

#import "MenuVC.h"
#import "LoginVC.h"
#import "LiveChanelVC.h"
#import "CellSection1.h"
#import "BetViewController.h"
#import "ModelManager.h"
#import "UpdateProfileViewController.h"

#import "HomeVC.h"

@interface MenuVC ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation MenuVC{
    NSArray *arrIconMenuSection1;
    NSArray *arrTitleMenuSection1;
    BOOL islogedin;
    __weak IBOutlet UILabel *lbl_ballUser;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return kStatusBarStyle;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSNumber *money = [[NSUserDefaults standardUserDefaults] valueForKey:@"money"] ;
    
    if ([money integerValue]==0)
    {
        lbl_ballUser.text = @"0 BALL" ;
    }
    else
    {
         lbl_ballUser.text = [NSString stringWithFormat:@"%ld BALL", [money integerValue]] ;
    }
   
    UIColor *color = [self colorFromHexString:@"e53935"];
    //_image.backgroundColor = color ;
    _tbViewMenu.delegate = self;
    _tbViewMenu.dataSource = self;
    _tbViewMenu.backgroundColor =  [self colorFromHexString:@"e53935"];
    NSString *urlImage = [[NSUserDefaults standardUserDefaults] valueForKey:@""] ;

    
    
    [self configViewController];
    
    // Do any additional setup after loading the view.
}
- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (void)configViewController{
    
//    [[NSUserDefaults standardUserDefaults] setValue:userDic forKey:@"isL"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
   // NSDictionary *dic = [[NSUserDefaults standardUserDefaults] valueForKey:@"isL"] ;
    NSString *str =  [[NSUserDefaults standardUserDefaults] valueForKey:@"fbID"] ;
    NSString *firstName = [[NSUserDefaults standardUserDefaults]valueForKey:@"firstName" ];
    NSString *lastName = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastName" ];
    if (str.length!=0)
    {
        
    }
    gUser.uFirstName = firstName ;
    gUser.uLastName = lastName ;
    if ([firstName length]>0 &&[lastName length]>0 )gUser.uUserName = [NSString stringWithFormat:@"%@ %@" , firstName , lastName] ;
    
    
    if (gUser ) {
        islogedin = true;
        arrTitleMenuSection1 = @[@"Trang chủ", @"Tin tức", @"Kênh live", @"Phòng chat", @"Dự đoán", @"Lịch thi đấu", @"Cập nhật tài khoản", @"Đăng xuất"];
        arrIconMenuSection1 = @[@"ic_home.png", @"ic_hot.png", @"ic_schedule.png", @"ic_contact.png"];
        _lblName.text = gUser.uUserName;
        [_imgAvatar sd_setImageWithURL:[NSURL URLWithString:gUser.uImgAvatarLink] placeholderImage:AVATAR_HOLDER];
    }else{
      
            islogedin = false;
            arrTitleMenuSection1 = @[@"Trang chủ", @"Tin tức", @"Kênh live", @"Phòng chat", @"Dự đoán", @"Lịch thi đấu", @"Cập nhật tài khoản"];
            arrIconMenuSection1 = @[@"ic_home.png", @"ic_hot.png", @"ic_schedule.png", @"ic_contact.png", @"ic_register"];
            _lblName.text = @"Đăng Nhập";
            [_imgAvatar setImage:AVATAR_HOLDER];
        }
       
    
    [_tbViewMenu reloadData];
    //    if (gArrCate.count == 0) {
    //        [self loadListCategoryFromServer];
    //    }
}
- (void)configViewControllerp : (NSString *) userName : (NSString*) url_image {
    
    //    [[NSUserDefaults standardUserDefaults] setValue:userDic forKey:@"isL"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    // NSDictionary *dic = [[NSUserDefaults standardUserDefaults] valueForKey:@"isL"] ;
    
    
    
    
        arrTitleMenuSection1 = @[@"Trang chủ", @"Tin tức", @"Kênh live", @"Phòng chat", @"Dự đoán", @"Lịch thi đấu", @"Cập nhật tài khoản", @"Đăng xuất"];
        arrIconMenuSection1 = @[@"ic_home.png", @"ic_hot.png", @"ic_schedule.png", @"ic_contact.png"];
        _lblName.text = userName;
        [_imgAvatar sd_setImageWithURL:[NSURL URLWithString:gUser.uImgAvatarLink] placeholderImage:AVATAR_HOLDER];
    
    
    [_tbViewMenu reloadData];
    //    if (gArrCate.count == 0) {
    //        [self loadListCategoryFromServer];
    //    }
}

- (void)loadListCategoryFromServer{
    //    [SVProgressHUD show];
    //    [NetworkManager getHomeSceenDataWithSuccessHandler:^(HomeLiveVideo *liveVideoObjReturn, NSArray *arrVideosReturn, NSArray *arrCategoriesReturn) {
    //        [SVProgressHUD dismiss];
    //        gArrCate = arrCategoriesReturn;
    //        [_tbViewMenu reloadData];
    //    } failure:^(NSString *err) {
    //        [SVProgressHUD dismiss];
    //    }];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *firstName = [[NSUserDefaults standardUserDefaults] valueForKey:@"firstName"] ;
    NSString *lastName = [[NSUserDefaults standardUserDefaults] valueForKey:@"lastName"] ;
    NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:@"email"] ;
    NSString *avatar = [[NSUserDefaults standardUserDefaults] valueForKey:@"avatar"] ;
    NSURL *url = [NSURL URLWithString:avatar] ;
    UIImage *imgThum = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]] ;
    _imgAvatar.image = imgThum;
    NSNumber *moneyS = [[NSUserDefaults standardUserDefaults] valueForKey:@"money"] ;
    //NSInteger money = [[NSUserDefaults standardUserDefaults] valueForKey:@"money"] ;
    NSInteger money = [moneyS integerValue] ;
    
   
    if (money==0)
    {
        lbl_ballUser.text = @"0 BALL" ;
    }
    else
    {
        lbl_ballUser.text = [NSString stringWithFormat:@"%ld BALL", money] ;
    }
    if ([firstName length] >0)
    {
        gUser.uFirstName = firstName ;
        gUser.uLastName = lastName ;
        gUser.uEmail = email ;
        gUser.uImgAvatarLink= avatar ;
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName] ;
        if ([fullName length]>0)[self configViewControllerp:fullName :avatar];
        else [self configViewController];
        
    }
    else
    {
        if (gUser) {
            
            if (!islogedin) {
                [self configViewController];
            }
        }else{
            if (islogedin) {
                [self configViewController];
            }
        }

    }
    //BOOL isCheck = [[NSUserDefaults standardUserDefaults]valueForKey:@"isL"];
    
   
    
    }

- (IBAction)onLogin:(id)sender {
    if (gUser) {
        [self gotoAccountInfoVC];
    }else{
        [self gotoLoginVC];
    }
    
}

- (void)gotoAccountInfoVC{
    
}

- (void)gotoLoginVC{
    LoginVC *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.revealViewController pushFrontViewController:loginVC animated:YES];
    
}



#pragma mark => TableView delegate and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrTitleMenuSection1.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if (indexPath.section == 0) {
    
  
    
    
    static NSString *cellSection1Identifier = @"CellSection1";
    CellSection1* cell = (CellSection1*)[tableView dequeueReusableCellWithIdentifier:cellSection1Identifier forIndexPath:indexPath];
    UIColor *color = [self colorFromHexString:@"2c305b"];
    [cell setBackgroundColor:color];
    cell.lblTitle.text = [arrTitleMenuSection1 objectAtIndex:indexPath.row];
//    [cell.iconMenu setImage:[UIImage imageNamed:arrIconMenuSection1[indexPath.row]]];
    [cell.iconMenu setImage:[UIImage imageNamed:@"ic_sport2.png"]];
    return cell;
    
    //    }else{
    //        static NSString *cellSection2Identifier = @"CellSection2";
    //        CellSection2* cell = (CellSection2*)[tableView dequeueReusableCellWithIdentifier:cellSection2Identifier forIndexPath:indexPath];
    //        Category* cateTemp = gArrCate[indexPath.row];
    //        cell.lblTitle.text = cateTemp.cName;
    //        return cell;
    //    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if (indexPath.section == 1) {
    //        Category* cateTemp = gArrCate[indexPath.row];
    //        [self goToCategory:cateTemp];
    //    }else if (indexPath.section == 0){
    //
    
    switch (indexPath.row) {
        case 0:
            [self goToHomeVC];
            break;
        case 2:
            [self gotoLiveChannel];
            break;
       
        case 4 :
            [self gotoBetView];
            break ;
        case 6:
            [self gotoUpdateProfile];
            
            break;
            
        case 7:
            [self logOut];
            break;

        
            //            case 2:
            //                [self goToScheduleVC];
            //                break;
            //            case 3:
            //                [self goToContactUsVC];
            //                break;
            //            case 4:
            //                [self onRegisterCell];
            //                break;
            //
        default:
            break;
    }
    // }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    //    if (section == 1) {
    //        return @"Chủ đề";
    //    }else{
    return nil;
    //   }
}



- (void)goToHomeVC{
    HomeVC *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.revealViewController pushFrontViewController:homeVC animated:YES];
}

- (void)gotoLiveChannel{
    LiveChanelVC *liveChanelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LiveChanelVC"];
    [self.revealViewController pushFrontViewController:liveChanelVC animated:YES];
}
- (void)gotoBetView{
    BetViewController *liveChanelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BetViewController"];
    [self.revealViewController pushFrontViewController:liveChanelVC animated:YES];
}
- (void)gotoUpdateProfile{
    NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"] ;
    if ([token length] >10)
    {
        UpdateProfileViewController *liveChanelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdateProfileViewController"];
        [self.revealViewController pushFrontViewController:liveChanelVC animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông Báo" message:@"Bạn phải đăng nhập " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        [alert show];
    }
    
}
- (void)logOut{
    gUser = nil;
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"token"];
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"firstName"];
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"lastName"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:0] forKey:@"money"] ;
    [[NSUserDefaults standardUserDefaults]synchronize];
    lbl_ballUser. text = @"0 BALL" ;
    
    [self configViewController];
    NSString *apiPath = @"logout?deviceToken=" ;
    NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"] ;
    NSString *strTokenReturn = [[NSUserDefaults standardUserDefaults] valueForKey:@"fcmtoken"] ;
    NSString *apiPathS = [NSString stringWithFormat:@"%@%@", apiPath, strTokenReturn] ;
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"token"] ;
               [[NSUserDefaults standardUserDefaults]setObject:NULL forKey:@"fcmtoken"];
   [ [NSUserDefaults standardUserDefaults]setObject:NULL forKey:@"avatar"];
                [[NSUserDefaults standardUserDefaults]synchronize];

//    
//    [ModelManager sendGetRequestWithAPIPath:apiPathS andToken:token successHandler:^(id idSuccess) {
//        if ([[Validator getSafeString:idSuccess[@"code"]] isEqualToString:@"0"]) {
//        
//            [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"token"] ;
//            [[NSUserDefaults standardUserDefaults]setObject:NULL forKey:@"fcmtoken"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            NSLog(@"") ;
//        }
//        
//    }failureHandler:^(NSString *strErrReturn) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [self.view makeToast:strErrReturn duration:2.0 position:CSToastPositionCenter];
//    }];
    
    
    
}

@end

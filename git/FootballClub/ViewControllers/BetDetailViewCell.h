//
//  BetDetailViewCell.h
//  FootballClub
//
//  Created by LuongTheDung on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BetDetailViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *question;
@property (weak, nonatomic) IBOutlet UITextView *noidungcauhoi;
@property (weak, nonatomic) IBOutlet UILabel *dapan;
@property (weak, nonatomic) IBOutlet UIButton *btn_DatCuoc;
@property (weak, nonatomic) IBOutlet UIView *color_view;

@end

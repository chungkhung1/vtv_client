
//

#import "LoginVC.h"
#import "HomeVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "User.h"
#import "ForgotPWViewController.h"

@interface LoginVC ()
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UITextField *tfUserName;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configViewController];
    // Do any additional setup after loading the view.
}

- (void)configViewController{
    [_btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)onLoginNormal:(id)sender {
    if (!_tfUserName.text.length || !_tfPassword.text.length) {
        [self.view makeToast:@"Vui lòng điền đầy đủ email/sđt và mật khẩu" duration:2.0 position:CSToastPositionCenter];
    
        return;
    }
    [self.view endEditing:YES];
    User *userTemp = [[User alloc] initWithDict:@{}];
    userTemp.uUserName = _tfUserName.text;
    userTemp.uPassword = _tfPassword.text;
    if ([Validator validateEmail:_tfUserName.text]) {
        userTemp.usAccountType = USER_TYPE_EMAIL;
    }else{
        userTemp.usAccountType = USER_TYPE_PHONE;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager loginWithUser:userTemp successHandler:^(NSString *strSuccessReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:userTemp.uUserName forKey:@"username"] ;
        [[NSUserDefaults standardUserDefaults] setValue:userTemp.uPassword forKey:@"pw"] ;
        [[NSUserDefaults standardUserDefaults]setValue:userTemp.uToken forKey:@"token"];
        [[NSUserDefaults standardUserDefaults]setObject:@"OK" forKey:@"islogin"];
        NSNumber *money = [NSNumber numberWithInteger: userTemp.uMoney] ;
        
        [[NSUserDefaults standardUserDefaults]setObject:money forKey:@"money"] ;
         [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSelectorOnMainThread:@selector(gotoHomeView) withObject:nil waitUntilDone:NO];
    } failureHandler:^(NSString *strErrReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:strErrReturn];
    }];
}

- (IBAction)onLoginWithFacebook:(id)sender {
    
    NSArray *permissions = @[@"public_profile",@"email"];
    // if the session is closed, then we open it here, and establish a handler for state changes
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [login logInWithReadPermissions:permissions fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.view makeToast:@"Có lỗi xảy ra khi kết nối với facebook!" duration:2.0 position:CSToastPositionCenter];
        } else if (result.isCancelled) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.view makeToast:@"Đăng nhập bằng facebook bị huỷ!" duration:2.0 position:CSToastPositionCenter];
        } else {
            
            if ([FBSDKAccessToken currentAccessToken])
            {
                User *userTemp = [[User alloc] initWithDict:@{}];
                userTemp.usAccountType = USER_TYPE_FACEBOOK;
                userTemp.uFacebookId = result.token.userID;
                userTemp.uFacebookToken = result.token.tokenString;
//                [[NSUserDefaults standardUserDefaults] setValue:result.token.tokenString forKey:@"token"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                [ModelManager registerWithUser:userTemp successHandler:^(NSString *strTokenReturn) {
                    userTemp.uToken = strTokenReturn;
                      [MBProgressHUD hideHUDForView:self.view animated:YES];

                    NSString *apiPath =@"user/profile";
                    
                    [[NSUserDefaults standardUserDefaults]setValue:strTokenReturn forKey:@"token"] ;
                    [[NSUserDefaults standardUserDefaults]synchronize] ;
                    [ModelManager sendGetRequestWithAPIPath:apiPath andToken:strTokenReturn successHandler:^(id idSuccess) {
                        if ([[Validator getSafeString:idSuccess[@"code"]] isEqualToString:@"0"]) {
                            NSDictionary *userDic = idSuccess[@"data"];
                         
                            NSString *firstName = [userDic valueForKey:@"firstName"] ;
                           NSString *lastName = [userDic valueForKey:@"lastName"] ;
                            NSString *email = [userDic valueForKey:@"email"];
                            NSString *avatar =  [userDic valueForKey:@"avatar"] ;
                            NSString *fbID = [userDic valueForKey:@"facebookID"] ;
                            NSNumber *money = [userDic valueForKey:@"money"] ;
                            
                            [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:@"firstName"];
                            [[NSUserDefaults standardUserDefaults]setObject:lastName forKey:@"lastName"];
                            [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"] ;
                            [[NSUserDefaults standardUserDefaults] setObject:avatar forKey:@"avatar"] ;
                            [[NSUserDefaults standardUserDefaults] setObject:fbID forKey:@"fbID"] ;
                            [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"isL"] ;
                            [[NSUserDefaults standardUserDefaults ]setObject:money forKey:@"money"];
                            [[NSUserDefaults standardUserDefaults] synchronize] ;
                            
                            gUser.uFirstName = firstName;
                            gUser.uLastName =lastName ;
                            gUser.uEmail =email ;
                            gUser.uImgAvatarLink = avatar ;
                            
       
                            }
                        
                    }failureHandler:^(NSString *strErrReturn) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [self.view makeToast:strErrReturn duration:2.0 position:CSToastPositionCenter];
                    }];

                    
                     [self performSelectorOnMainThread:@selector(gotoHomeView) withObject:nil waitUntilDone:NO];
//                    [ModelManager loginWithUser:userTemp successHandler:^(NSString *strSuccessReturn) {
//                      
//                       
//                    } failureHandler:^(NSString *strErrReturn) {
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
//                        [self.view makeToast:strErrReturn duration:2.0 position:CSToastPositionCenter];
//                    }];
                } failureHandler:^(NSString *strErrReturn) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [self.view makeToast:strErrReturn duration:2.0 position:CSToastPositionCenter];
                }];
            
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.view makeToast:@"Truy cập facebook bị từ chối!" duration:2.0 position:CSToastPositionCenter];
        
            }
        }
        
    }];

    
}

- (void)gotoHomeView{
    HomeVC *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.revealViewController pushFrontViewController:homeVC animated:YES];
}

- (IBAction)onForgotPass:(id)sender {
    ForgotPWViewController *forot = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPWViewController"] ;
    [self.revealViewController pushFrontViewController:forot animated:YES];
   
    
}


@end

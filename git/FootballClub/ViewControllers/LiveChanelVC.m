//

//

#import "LiveChanelVC.h"
#import "LiveChanelTbCell.h"
#import "PlayDetailVideoVC.h"

@interface LiveChanelVC ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbVideos;

@end

@implementation LiveChanelVC{
NSMutableArray *arrMulVideos;
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return kStatusBarStyle;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configViewController];
    [self getDataFromSever];
    
    // Do any additional setup after loading the view.
}

- (void)configViewController{
    [_btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    arrMulVideos = [[NSMutableArray alloc] init];
    _tbVideos.delegate = self;
    _tbVideos.dataSource = self;
    _tbVideos.estimatedRowHeight = 44;
    _tbVideos.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)getDataFromSever{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager getListVideoInLiveChanelWithSuccessHandler:^(NSArray *arrVideosReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        arrMulVideos = arrVideosReturn.mutableCopy;
        [_tbVideos reloadData];
    } failure:^(NSString *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
    }];
 
}

#pragma mark =>TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMulVideos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"LiveChanelTbCell";
    LiveChanelTbCell *cell = (LiveChanelTbCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Video *videoTemp = [arrMulVideos objectAtIndex:indexPath.row];
    [cell setupDataCellWithVideoObj:videoTemp];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *strInput = [[NSUserDefaults standardUserDefaults] valueForKey:@"firstName"];
    if (!gUser&(strInput == nil)) {
        [Util showAlert:@"Vui lòng đăng nhập để xem video" withTitle:APP_NAME atController:self];
        return;
    }
    Video *tempVideo = [arrMulVideos objectAtIndex:indexPath.row];
    [self getVideoLinkofVideoObj:tempVideo];
   
}

- (void)getVideoLinkofVideoObj:(Video*)videoObj{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   [ModelManager getLinkVideoWithVideoId:videoObj.videoId successHandler:^(Video *videoObjReturn) {
       [MBProgressHUD hideHUDForView:self.view animated:YES];
       [self gotoPlayVideo:videoObjReturn];
       
   } failureHandler:^(NSString *err, NSString *status) {
       [MBProgressHUD hideHUDForView:self.view animated:YES];
       if ([status isEqualToString:@"0"]) {
           [self askUserBuyVideo:videoObj];
       }else{
           [self.view makeToast:err duration:2.0 position:CSToastPositionBottom];
       }
   }];
}

- (void)askUserBuyVideo:(Video*)videoObj{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Mua video" message:@"Xác nhận mua video để tiếp tục xem"preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *btOK = [UIAlertAction actionWithTitle:@"Đồng ý" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self buyVideoWithVideoObj:videoObj];
    }];
    
    UIAlertAction *btCancel = [UIAlertAction actionWithTitle:@"Huỷ" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:btOK];
    [alert addAction:btCancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)buyVideoWithVideoObj:(Video*)videoObj{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager buyVideoWithVideoId:videoObj.videoId successHandler:^(Video *videoObjectReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [self gotoPlayVideo:videoObjectReturn];
    } failureHandler:^(NSString *err, NSString *status) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionBottom];
    }];

}

- (void)gotoPlayVideo:(Video*)videoObj{
    PlayDetailVideoVC *playVideoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayDetailVideoVC"];
    playVideoVC.videoObj = videoObj;
    [self.navigationController pushViewController:playVideoVC animated:YES];
}


@end

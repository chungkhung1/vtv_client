//
//  BetTableViewCell.m
//  FootballClub
//
//  Created by LuongTheDung on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "BetTableViewCell.h"

@implementation BetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configWithMatch:(MatchObj *)match colorStatuc:(BOOL) colorStatus {
    self.lblAway.text = match.awayClub;
    self.lblHome.text = match.homeClub;
    self.lblTime.text = match.matchDate;
    self.btn_bet.userInteractionEnabled = false ;
    if (colorStatus == false)
    {
        UIColor *color = [UIColor colorWithRed:0.5f green:0.8f blue:0.8f alpha:1.0f];
        [self.colorView setBackgroundColor:color] ;
    }
   // self.col
    
    
}

- (IBAction)btnBet:(id)sender {
}
@end

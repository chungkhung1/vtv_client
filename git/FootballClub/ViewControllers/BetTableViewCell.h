//
//  BetTableViewCell.h
//  FootballClub
//
//  Created by LuongTheDung on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchObj.h"
#import "DViewWithBorder.h"
@interface BetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image_home;
@property (weak, nonatomic) IBOutlet UIImageView *image_gúet;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblAway;
@property (weak, nonatomic) IBOutlet UILabel *lblHome;
@property (weak, nonatomic) IBOutlet UIButton *btn_bet;
@property (weak, nonatomic) IBOutlet UIView *colorView;

- (void) configWithMatch:(MatchObj *)match colorStatuc:(BOOL) colorStatus ;

@end

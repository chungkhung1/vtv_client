//

//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"
#import "Video.h"

@interface PlayDetailVideoVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet YTPlayerView *viewYTPlayerVideo;
@property (strong, nonatomic) Video* videoObj;


@end

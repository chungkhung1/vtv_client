//
//  CellSection1.h
//  KisimeTV
//
//  Created by Luong The Dung on 11/10/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellSection1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

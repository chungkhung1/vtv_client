//
//  ChooseBetVC.m
//  FootballClub
//
//  Created by Chung BD on 12/27/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "ChooseBetVC.h"

@interface ChooseBetVC ()

@end

@implementation ChooseBetVC

#pragma mark - life cycle of view
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public methods
- (void) initView {
    
}

+ (instancetype) instantiate {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChooseBetVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChooseBetVC"];
    return vc;
}
#pragma mark - Private methods

#pragma mark - IBAction
- (IBAction)touchUpInside_btn:(id)sender {
    
}

- (IBAction)touchUpInside_btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

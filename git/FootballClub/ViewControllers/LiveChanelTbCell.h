//


#import <UIKit/UIKit.h>

@interface LiveChanelTbCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVideo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleVideo;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoViewCount;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoPrice;
@property (weak, nonatomic) IBOutlet UIView *viewContainImgVideo;

@property (strong, nonatomic) Video* cellVideo;

- (void)setupDataCellWithVideoObj:(Video*)videoObj;
@end

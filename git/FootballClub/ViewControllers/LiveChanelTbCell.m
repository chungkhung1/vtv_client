//
//  LiveChanelTbCell.m


#import "LiveChanelTbCell.h"

@implementation LiveChanelTbCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _viewContainImgVideo.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _viewContainImgVideo.layer.borderWidth = 1;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupDataCellWithVideoObj:(Video *)videoObj{
    _cellVideo = videoObj;
    [_imgVideo sd_setImageWithURL:[NSURL URLWithString:videoObj.imageLink] placeholderImage:IMAGE_HOLDER];
    _lblTitleVideo.text = videoObj.name;
    _lblVideoPrice.text = [NSString stringWithFormat:@"%.1f", videoObj.fee];
    if (videoObj.fee == 0) {
        _lblVideoPrice.text = @"Free";
    }
    _lblVideoViewCount.text = [NSString stringWithFormat:@"%d", videoObj.order];
}

@end

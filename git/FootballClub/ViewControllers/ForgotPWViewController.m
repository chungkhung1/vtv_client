//
//  ForgotPWViewController.m
//  FootballClub
//
//  Created by LuongTheDung on 12/28/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "ForgotPWViewController.h"

@interface ForgotPWViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txt_Email;

@end

@implementation ForgotPWViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txt_Email.delegate = self ;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnBack:(id)sender {
    [self.revealViewController popoverPresentationController];
    
}
- (IBAction)btnReset:(id)sender {
        NSDictionary *param ;
    param = @{@"email": _txt_Email.text } ;
    
    NSString *api_Path = @"auth/resetPassword" ;
    
    [ModelManager sendPOSTRequestViaFormDataWithAPIPath:api_Path Param:param successHandler:^(id idSuccess) {
        UIAlertView *infor = [[UIAlertView alloc ] initWithTitle:@"Thông báo" message:[idSuccess valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil] ;
        [infor show ];
    } failureHandler:^(NSString *err) {
        
        UIAlertView *infor = [[UIAlertView alloc ] initWithTitle:@"Thông báo" message:err delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil] ;
        [infor show ];
        
    }];
    
    
}

@end

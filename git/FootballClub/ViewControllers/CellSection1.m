//
//  CellSection1.m
//  KisimeTV
//
//  Created by Luong The Dung on 11/10/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "CellSection1.h"

@implementation CellSection1

- (void)awakeFromNib {
    [super awakeFromNib];
    _lblTitle.textColor = [UIColor lightGrayColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

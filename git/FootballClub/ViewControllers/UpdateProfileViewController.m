//
//  UpdateProfileViewController.m
//  FootballClub
//
//  Created by LuongTheDung on 12/27/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "UpdateProfileViewController.h"
#import "ModelManager.h"
#import "HomeVC.h"

@interface UpdateProfileViewController () <UITextFieldDelegate>


@end

@implementation UpdateProfileViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    _txt_ten.delegate = self ;
    _txtPhone.delegate = self ;
    _txt_email.delegate = self  ;
    _txt_hodem.delegate = self ;
    NSString *email = [[NSUserDefaults standardUserDefaults]valueForKey:@"email"];
    NSString *phone = [[NSUserDefaults standardUserDefaults]valueForKey:@"phone"];
    NSString *firstName = [[NSUserDefaults standardUserDefaults] valueForKey:@"firstName"];
    NSString *lastName = [[NSUserDefaults standardUserDefaults]valueForKey:@"lastName" ];
    _txt_ten.text = firstName ;
    _txt_hodem .text = lastName ;
    _txtPhone.text = phone ;
    _txt_email.text = email ; 
    //[self configViewController] ;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)click:(id)sender {
    [_btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnBack:(id)sender {
}


- (IBAction)btnUpdate:(id)sender {
    
   NSString *token =  [[NSUserDefaults standardUserDefaults] valueForKey:@"token"] ;
    [ModelManager updateProfile:token firstName:_txt_ten.text lastName:_txt_hodem.text email:_txt_email.text phone:_txtPhone.text successHandler:^(NSDictionary *userObjReturn) {
        NSString *inform = [userObjReturn valueForKey:@"message"] ;
        
        NSDictionary *dictationData = [userObjReturn valueForKey:@"data"];
        if ([dictationData count] >= 8)
        {
            [[NSUserDefaults standardUserDefaults]setObject:[dictationData valueForKey:@"avartar"] forKey:@"avatar"];
            [[NSUserDefaults standardUserDefaults]setObject:[dictationData valueForKey:@"facebookID"] forKey:@"facebookID"];
            [[NSUserDefaults standardUserDefaults]setObject:[dictationData valueForKey:@"phone"] forKey:@"phone"];
            [[NSUserDefaults standardUserDefaults]setObject:[dictationData valueForKey:@"firstName"] forKey:@"firstName"];
            [[NSUserDefaults standardUserDefaults]setObject:[dictationData valueForKey:@"lastName"] forKey:@"lastName"];
            [[NSUserDefaults standardUserDefaults]setObject:[dictationData valueForKey:@"money"] forKey:@"money"];
            [[NSUserDefaults standardUserDefaults]setObject:[dictationData valueForKey:@"email"] forKey:@"email"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông Báo" message:inform delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        [alert show];
        
        NSLog(@"Log here") ;
        
    } failureHandler:^(NSString *strErrReturn) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:strErrReturn duration:2.0 position:CSToastPositionCenter];
    }] ;

    
    
}
- (IBAction)btnAction:(id)sender {
    
    HomeVC *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.revealViewController pushFrontViewController:homeVC animated:YES];
// [self.view present]
}
@end

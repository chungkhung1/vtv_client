//
//  DCycleImgView.m
//  FootballClub
//
//  Created by Luong The Dung on 12/24/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "DCycleImgView.h"

@implementation DCycleImgView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customizeView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customizeView];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self customizeView];
}

- (void)customizeView{
    self.layer.cornerRadius = self.bounds.size.width/2;
    self.layer.masksToBounds = YES;
}

@end



#import "DTextFieldWithBorder.h"

@implementation DTextFieldWithBorder

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIView *viewPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        [self setLeftViewMode:UITextFieldViewModeAlways];
        [self setLeftView:viewPadding];
        [self customizeView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIView *viewPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        [self setLeftViewMode:UITextFieldViewModeAlways];
        [self setLeftView:viewPadding];
        [self customizeView];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self customizeView];
}

- (void)customizeView{
    
    self.layer.cornerRadius = self.bounds.size.height/2;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.layer.borderWidth = 1;

}


@end

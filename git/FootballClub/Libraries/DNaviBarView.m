
//

#import "DNaviBarView.h"

@implementation DNaviBarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customizeView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customizeView];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self customizeView];
}

- (void)customizeView{
    self.backgroundColor = COLOR_BG_NAVIBAR;
}

@end

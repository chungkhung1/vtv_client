
//


#import "ModelManager.h"
#import <AFNetworking/AFNetworking.h>

#define CALL_API_ERROR @"Có lỗi kết nối với máy chủ"

@implementation ModelManager

#pragma BetViewController 
+ (void)getListQuestion:(NSString*)userToken successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure
{
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, BASE_BET];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 20;
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //
    if (userToken.length > 0) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", userToken] forHTTPHeaderField:@"Authorization"];
    }
    
   // NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, apiPath];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //        if ([[Validator getSafeString:responseObject[@"code"]] isEqualToString:@"0"]) {
        success(responseObject);
        //        }else{
        //            failure([Validator getSafeString:responseObject[@"message"]]);
        //        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        failure(CALL_API_ERROR);
    }];
}
+ (void)getListOneQuestion:(NSString*)userToken questionID :(NSString*) questionID successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure
{
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, questionID];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 20;
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //
    if (userToken.length > 0) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", userToken] forHTTPHeaderField:@"Authorization"];
    }
    
    // NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, apiPath];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //        if ([[Validator getSafeString:responseObject[@"code"]] isEqualToString:@"0"]) {
        success(responseObject);
        //        }else{
        //            failure([Validator getSafeString:responseObject[@"message"]]);
        //        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        failure(CALL_API_ERROR);
    }];
}

+ (void)updateProfile:(NSString *)userToken firstName:(NSString *)firstName lastName:(NSString*)lastName email:(NSString *)email phone :(NSString*) phone successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, UPDATEPROFILE];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFJSONRequestSerializer *serrilize = [AFJSONRequestSerializer serializer];
    [serrilize setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [serrilize setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSString *tokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"];
    if (tokenString.length > 0) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", tokenString] forHTTPHeaderField:@"Authorization"];
    }
    NSDictionary *param;
    
    param = @{@"firstName": firstName ,
              @"lastName" : lastName ,
              @"email" : email,
              @"phone":phone};
    NSArray *arrKey = [param allKeys];
    NSMutableArray *arrDataOfKey = [[NSMutableArray alloc] init];
    for (NSString *keyTemp in arrKey) {
        NSMutableData *dataOfKey = [[NSMutableData alloc] init];
        [dataOfKey appendData:[param[keyTemp] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [arrDataOfKey addObject:dataOfKey];
    }
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (int i = 0; i < arrKey.count; i++) {
            [formData appendPartWithFormData:arrDataOfKey[i] name:arrKey[i]];
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[Validator getSafeString:responseObject[@"code"]] isEqualToString:@"0"]) {
          //  [[NSUserDefaults standardUserDefaults ] setValue:re forKey:<#(nonnull NSString *)#>]
            success(responseObject);
        }else{
            failure([Validator getSafeString:responseObject[@"message"]]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        failure(CALL_API_ERROR);
        
    }];
    
    
}



+ (void)betMatchWithUserToken:(NSString *)userToken questionID:(NSNumber *)quesID answerID:(NSNumber*)answer betValue:(NSString *)value successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, BET_MATCH];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    AFJSONRequestSerializer *serrilize = [AFJSONRequestSerializer serializer];
    [serrilize setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [serrilize setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSString *tokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"];
    if (tokenString.length > 0) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", tokenString] forHTTPHeaderField:@"Authorization"];
    }
    NSDictionary *params;
    
    NSString *quest_id_string = [quesID stringValue] ;
    NSString *answer_id_string = [answer stringValue] ;
    
    NSDictionary *param;
    
      param = @{@"questionID": quest_id_string ,
                  @"answerID" : answer_id_string ,
                  @"value" : value};
    
    NSArray *arrKey = [param allKeys];
    NSMutableArray *arrDataOfKey = [[NSMutableArray alloc] init];
    for (NSString *keyTemp in arrKey) {
        NSMutableData *dataOfKey = [[NSMutableData alloc] init];
        [dataOfKey appendData:[param[keyTemp] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [arrDataOfKey addObject:dataOfKey];
    }
    //  NSLog(@"%@", [NSDate date]);
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (int i = 0; i < arrKey.count; i++) {
            [formData appendPartWithFormData:arrDataOfKey[i] name:arrKey[i]];
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[Validator getSafeString:responseObject[@"code"]] isEqualToString:@"0"]) {
            success(responseObject);
        }else{
            failure([Validator getSafeString:responseObject[@"message"]]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        failure(CALL_API_ERROR);
    }];

    
}


+ (void)sendGetRequestWithAPIPath:(NSString *)apiPath andToken:(NSString*)token successHandler:(void (^)(id))success failureHandler:(void (^)(NSString *))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
   // manager.requestSerializer.timeoutInterval = 20;
   [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    
    if (token.length > 0) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", token] forHTTPHeaderField:@"Authorization"];
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, apiPath];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        if ([[Validator getSafeString:responseObject[@"code"]] isEqualToString:@"0"]) {
            success(responseObject);
//        }else{
//            failure([Validator getSafeString:responseObject[@"message"]]);
//        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    
        NSLog(@"%@", error);
        failure(CALL_API_ERROR);
    }];
}

+ (void)sendPOSTRequestViaFormDataWithAPIPath:(NSString *)apiPath Param:(NSDictionary *)param successHandler:(void (^)(id))success failureHandler:(void (^)(NSString *))failure{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_DOMAIN, apiPath];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    AFJSONRequestSerializer *serrilize = [AFJSONRequestSerializer serializer];
    [serrilize setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serrilize setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSString *tokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"];
    if (tokenString.length > 0) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", tokenString] forHTTPHeaderField:@"Authorization"];
    }


    NSArray *arrKey = [param allKeys];
    NSMutableArray *arrDataOfKey = [[NSMutableArray alloc] init];
    for (NSString *keyTemp in arrKey) {
        NSMutableData *dataOfKey = [[NSMutableData alloc] init];
        [dataOfKey appendData:[param[keyTemp] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [arrDataOfKey addObject:dataOfKey];
    }
  //  NSLog(@"%@", [NSDate date]);
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (int i = 0; i < arrKey.count; i++) {
            [formData appendPartWithFormData:arrDataOfKey[i] name:arrKey[i]];
        }
 
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[Validator getSafeString:responseObject[@"code"]] isEqualToString:@"0"]) {
            success(responseObject);
        }else{
            failure([Validator getSafeString:responseObject[@"message"]]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        failure(CALL_API_ERROR);
    }];
}

// API Authen
+ (void)loginWithUser:(User *)userObj successHandler:(void (^)(NSString *))success failureHandler:(void (^)(NSString *))failure{
    NSString *strAPIPath = @"";
    NSDictionary *params;
    
    if ([userObj.usAccountType isEqualToString:USER_TYPE_EMAIL]) {
        strAPIPath = @"auth/loginEmail";
        params = @{@"email" : userObj.uUserName,
                   @"password" : userObj.uPassword,
                   @"deviceToken" : [NSString stringWithFormat:@"FCM %@", gDeviceToken]};
    } else if ([userObj.usAccountType isEqualToString:USER_TYPE_PHONE]) {
        strAPIPath = @"auth/loginPhone";
        params = @{@"phone" : userObj.uUserName,
                   @"password" : userObj.uPassword,
                   @"deviceToken" : [NSString stringWithFormat:@"FCM %@", gDeviceToken]};
    }else if ([userObj.usAccountType isEqualToString:USER_TYPE_FACEBOOK]) {
        strAPIPath = @"auth/loginFacebook";
        params = @{@"facebookID" : userObj.uFacebookId,
                   @"token" : userObj.uToken,
                   @"deviceToken" : [NSString stringWithFormat:@"FCM %@", gDeviceToken]};
    }else{
        failure(@"Lỗi hệ thống. Vui lòng liên hệ admin để báo lỗi, cảm ơn!");
        return;
    }
    
    NSLog(@"Log in with user") ;
    [self sendPOSTRequestViaFormDataWithAPIPath:strAPIPath Param:params successHandler:^(id idSuccess) {
                gUser = userObj;
                gUser.uToken = [Validator getSafeString:idSuccess[@"token"]];
//
        NSString *apiPath =@"user/profile";
     
        [self sendGetRequestWithAPIPath:apiPath andToken:gUser.uToken successHandler:^(id idSuccess) {
                    if ([[Validator getSafeString:idSuccess[@"code"]] isEqualToString:@"0"]) {
                        NSDictionary *userDic = idSuccess[@"data"];
                        User *userTemp = [[User alloc] initWithDict:userDic];
                        
                        userTemp.uToken = gUser.uToken;
                        userTemp.usAccountType = gUser.usAccountType;
                        userTemp.uUserName = gUser.uUserName;
                        gUser = userTemp;
                        success(gUser.uToken);
                    }else{
                        failure([Validator getSafeString:idSuccess[@"message"]]);
                    }
           
        } failureHandler:^(NSString *err) {
            gUser = nil;
            failure(err);
        }];
        

    } failureHandler:^(NSString *err) {
        failure(err);
    }];
}

+ (void)registerWithUser:(User *)userObj successHandler:(void (^)(NSString *))success failureHandler:(void (^)(NSString *))failure{
    NSString *strAPIPath = @"";
    NSDictionary *params;
    if ([userObj.usAccountType isEqualToString:USER_TYPE_EMAIL]) {
        strAPIPath = @"auth/registerEmaill";
        params = @{@"email" : userObj.uUserName,
                   @"password" : userObj.uPassword,
                   @"firstName" : userObj.uFirstName,
                   @"lastName" : userObj.uLastName,
                   @"deviceToken" : [NSString stringWithFormat:@"FCM %@", gDeviceToken]};
    } else if ([userObj.usAccountType isEqualToString:USER_TYPE_PHONE]) {
        strAPIPath = @"auth/registerPhone";
        params = @{@"phone" : userObj.uUserName,
                   @"password" : userObj.uPassword,
                   @"firstName" : userObj.uFirstName,
                   @"lastName" : userObj.uLastName,
                   @"deviceToken" : [NSString stringWithFormat:@"FCM %@", gDeviceToken]};
    }else if ([userObj.usAccountType isEqualToString:USER_TYPE_FACEBOOK]) {
        strAPIPath = @"auth/loginFacebook";
        params = @{@"facebookID" : userObj.uFacebookId,
                   @"token" : userObj.uFacebookToken,
                   @"deviceToken" : [NSString stringWithFormat:@"FCM %@", gDeviceToken]};
    }else{
        failure(@"Lỗi hệ thống. Vui lòng liên hệ admin để báo lỗi, cảm ơn!");
        return;
    }
    NSLog(@"Login with user") ;
    [self sendPOSTRequestViaFormDataWithAPIPath:strAPIPath Param:params successHandler:^(id idSuccess) {
        
        success([Validator getSafeString:idSuccess[@"token"]]);
    } failureHandler:^(NSString *err) {
        failure(err);
    }];

}
+ (void)getListImage:(void (^)(NSArray *))success failure:(void (^)(NSString *))failure{
    [self sendGetRequestWithAPIPath:@"slide/1" andToken:@"" successHandler:^(id idSuccess) {
        if ([[Validator getSafeString:idSuccess[@"code"]] isEqualToString:@"0"]) {
            NSArray *arrVideoDics = idSuccess[@"data"];
            NSMutableArray *arrMulVideos = [[NSMutableArray alloc] init];
            for (NSDictionary *videoDic in arrVideoDics) {
                Video *videoTemp = [[Video alloc] initWithDict:videoDic];
                [arrMulVideos addObject:videoTemp];
            }
            success(arrMulVideos.copy);
        }else{
            failure([Validator getSafeString:idSuccess[@"message"]]);
        }
        
    } failureHandler:^(NSString *err) {
        failure(err);
    }];
    
}

+ (void)getListVideoInLiveChanelWithSuccessHandler:(void (^)(NSArray *))success failure:(void (^)(NSString *))failure{
    [self sendGetRequestWithAPIPath:@"video/" andToken:@"" successHandler:^(id idSuccess) {
        if ([[Validator getSafeString:idSuccess[@"code"]] isEqualToString:@"0"]) {
            NSArray *arrVideoDics = idSuccess[@"data"];
            NSMutableArray *arrMulVideos = [[NSMutableArray alloc] init];
            for (NSDictionary *videoDic in arrVideoDics) {
                Video *videoTemp = [[Video alloc] initWithDict:videoDic];
                [arrMulVideos addObject:videoTemp];
            }
            success(arrMulVideos.copy);
        }else{
            failure([Validator getSafeString:idSuccess[@"message"]]);
        }
        
    } failureHandler:^(NSString *err) {
        failure(err);
    }];

}

+ (void)getLinkVideoWithVideoId:(NSString *)videoId successHandler:(void (^)(Video *))success failureHandler:(void (^)(NSString *, NSString*))failure{
    NSString *apiPath = [NSString stringWithFormat:@"video/link/%@", videoId];
    NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"] ;
    
    if (gUser.uToken == nil) gUser.uToken = token;
    [self sendGetRequestWithAPIPath:apiPath andToken:token successHandler:^(id idSuccess) {
        if ([[Validator getSafeString:idSuccess[@"code"]] isEqualToString:@"0"]) {
            Video *tempVideo = [[Video alloc] initWithDict:idSuccess[@"data"]];
            success(tempVideo);
        }else{
            failure([Validator getSafeString:idSuccess[@"message"]], @"1");
        }
    } failureHandler:^(NSString *err) {
         failure(err, @"0");
           }];
}

+ (void)buyVideoWithVideoId:(NSString *)videoId successHandler:(void (^)(Video *))success failureHandler:(void (^)(NSString *, NSString *))failure{
    NSString *apiPath = [NSString stringWithFormat:@"video/pay/%@", videoId];
    NSString *str_token = [[NSUserDefaults standardUserDefaults] valueForKey:@"token"] ;
    [self sendGetRequestWithAPIPath:apiPath andToken:str_token successHandler:^(id idSuccess) {
        if ([[Validator getSafeString:idSuccess[@"code"]] isEqualToString:@"0"]) {
            Video *tempVideo = [[Video alloc] initWithDict:idSuccess[@"data"]];
            NSNumber *money = [tempVideo valueForKey:@"userMoney"] ;
            [[NSUserDefaults standardUserDefaults] setObject:money forKey:@"money"] ;
            [[NSUserDefaults standardUserDefaults] synchronize] ;
            success(tempVideo);
        }else{
            failure([Validator getSafeString:idSuccess[@"message"]], @"1");
        }
    } failureHandler:^(NSString *err) {
        failure(err, @"0");
    }];

}



@end

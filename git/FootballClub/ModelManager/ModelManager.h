//


#import <Foundation/Foundation.h>
#import "User.h"
#import "Video.h"
@interface ModelManager : NSObject

+ (void)sendGetRequestWithAPIPath:(NSString*)apiPath andToken:(NSString*)token successHandler:(void (^)(id idSuccess))success failureHandler:(void (^)(NSString *err))failure;

+ (void)sendPOSTRequestViaFormDataWithAPIPath:(NSString*)apiPath Param:(NSDictionary*)param successHandler:(void (^)(id idSuccess))success failureHandler:(void (^)(NSString* err))failure;

+ (void)loginWithUser:(User*)userObj successHandler:(void (^)(NSString *strSuccessReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure;

+ (void)registerWithUser:(User*)userObj successHandler:(void (^)(NSString *strTokenReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure;

+ (void)getUserInfoWithToken:(NSString*)userToken successHandler:(void (^)(User *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure;

+ (void)getListVideoInLiveChanelWithSuccessHandler:(void (^)(NSArray *arrVideosReturn))success failure:(void (^)(NSString *err))failure;

+ (void)getLinkVideoWithVideoId:(NSString*)videoId successHandler:(void (^)(Video* videoObjReturn))success failureHandler:(void (^)(NSString *err, NSString *status))failure;

+ (void)buyVideoWithVideoId:(NSString*)videoId successHandler:(void (^)(Video* videoObjectReturn))success failureHandler:(void (^)(NSString* err, NSString *status))failure;
+ (void)getListImage:(void (^)(NSArray *))success failure:(void (^)(NSString *))failure ;

+ (void)updateProfile:(NSString *)userToken firstName:(NSString *)firstName lastName:(NSString*)lastName email:(NSString *)email phone :(NSString*) phone successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure;

#pragma mark BetViewController 
+ (void)getListQuestion:(NSString*)userToken successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure;

+ (void)betMatchWithUserToken:(NSString*)userToken questionID:(NSNumber *)quesID answerID:(NSNumber *)answer betValue:(NSString *)value successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure;
+ (void)updateProfile:(NSString *)userToken firstName:(NSString *)firstName lastName:(NSString*)lastName email:(NSString *)email phone :(NSString*) phone successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure ;

+ (void)getListOneQuestion:(NSString*)userToken questionID :(NSString*) questionID successHandler:(void (^)(NSDictionary *userObjReturn))success failureHandler:(void (^)(NSString *strErrReturn))failure ;

@end

//
//  Video.h
//  FootballClub

//

#import <Foundation/Foundation.h>

@interface Video : NSObject
@property (strong, nonatomic) NSString *videoId;
@property (assign, nonatomic) int order;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *imageLink;
@property (assign, nonatomic) float fee;
@property (strong, nonatomic) NSString *linkVideo;
- (instancetype)initWithDict:(NSDictionary*)dict;
@end



//

#import <Foundation/Foundation.h>
#define USER_TYPE_PHONE     @"1"
#define USER_TYPE_EMAIL     @"2"
#define USER_TYPE_FACEBOOK  @"3"
#define kSaveUserToNsUserDefault @"SAVE_USER_TO_NS_USER_DEFAULT"

@interface User : NSObject
@property (strong,nonatomic) NSString *usAccountType;
@property (strong,nonatomic) NSString *uLastName;
@property (strong,nonatomic) NSString *uFirstName;
@property (strong,nonatomic) NSString *uUserName;
@property (strong,nonatomic) NSString *uFacebookId;
@property (strong, nonatomic) NSString *uPassword;
@property (strong,nonatomic) NSString *uFacebookToken;
@property (strong, nonatomic) NSString *uImgAvatarLink;
@property (strong, nonatomic) NSString *uStatus;
@property (strong, nonatomic) NSString *uToken;
@property (assign, nonatomic) NSInteger uMoney;
@property (strong, nonatomic) NSString *uEmail;
@property (strong, nonatomic) NSString *uPhone;
@property (strong, nonatomic) NSString *uBirthDay;
@property (strong, nonatomic) NSString *uGender;
@property (strong, nonatomic) NSString *uProvince;
@property (strong, nonatomic) NSString *uProvince_id;
//@property (assign, nonatomic) NSInteger
@property ( assign , nonatomic )BOOL isLogin ; 

- (instancetype)initWithDict:(NSDictionary*)dict;
@end




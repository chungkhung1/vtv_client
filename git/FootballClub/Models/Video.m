//
//  Video.m

//

#import "Video.h"

@implementation Video

- (instancetype)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        _videoId = [Validator getSafeString:dict[@"id"]];
        _order = [[Validator getSafeString:dict[@"order"]] intValue];
        _name = [Validator getSafeString:dict[@"name"]];
        _fee = [Validator getSafeFloat:dict[@"fee"]];
        _imageLink =  [Validator getSafeString:dict[@"image"]];
        _linkVideo = [Validator getSafeString:dict[@"link"]];
    }
    return self;
}
@end


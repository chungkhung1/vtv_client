//
//  MatchObj.m
//  FootballClub
//
//  Created by Chung BD on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import "MatchObj.h"

@implementation MatchObj
+ (instancetype) instantiateWithDictionary:(NSDictionary *)dic {
    MatchObj *match = [MatchObj new];
    
    match.matchID = dic[@"matchID"];
    match.matchName = dic[@"matchName"];
    match.matchDate = dic[@"matchDate"];
    match.homeClub = dic[@"homeClub"];
    match.awayClub = dic[@"awayClub"];
    
    return match;
}
@end

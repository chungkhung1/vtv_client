
//

#import "User.h"

@implementation User
- (instancetype)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        _uImgAvatarLink = [Validator getSafeString:dict[@"avatar"]];
        _uBirthDay = [Validator getSafeString:dict[@"birthday"]];
        _uEmail = [Validator getSafeString:dict[@"email"]];
        _uFacebookId = [Validator getSafeString:dict[@"facebookID"]];
        _uFirstName = [Validator getSafeString:dict[@"firstName"]];
        _uGender = [Validator getSafeString:dict[@"gender"]];
        _uLastName = [Validator getSafeString:dict[@"lastName"]];
        _uMoney = [Validator getSafeInterger:dict[@"money"]];
        _uPhone = [Validator getSafeString:dict[@"phone"]];
        _uProvince = [Validator getSafeString:dict[@"province"]];
        _uProvince_id = [Validator getSafeString:dict[@"province_id"]];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder
{
    //[coder encodeObject:estateId forKey:@"locationId"];
    [coder encodeObject:_uImgAvatarLink forKey:@"userPhone"];
    [coder encodeObject:_uBirthDay forKey:@"userPassword"];
    [coder encodeObject:_uEmail forKey:@"userEmail"];
    [coder encodeObject:_uFacebookId forKey:@"userImage"];
    [coder encodeObject:_uFirstName forKey:@"userAddress"];
    [coder encodeObject:_uGender forKey:@"userBirthday"];
    [coder encodeObject:_uLastName forKey:@"userFullName"];
    [coder encodeObject:_uPhone forKey:@"userToken"];
    
}


@end

//
//  MatchObj.h
//  FootballClub
//
//  Created by Chung BD on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatchObj : NSObject
@property (strong, nonatomic) NSString *matchID;
@property (strong, nonatomic) NSString *matchName;
@property (strong, nonatomic) NSString *matchDate;
@property (strong, nonatomic) NSString *homeClub;
@property (strong, nonatomic) NSString *awayClub;

+ (instancetype) instantiateWithDictionary:(NSDictionary *)dic;
@end

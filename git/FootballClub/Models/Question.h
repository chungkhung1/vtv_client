//
//  Question.h
//  FootballClub
//
//  Created by LuongTheDung on 12/26/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject
@property (strong, nonatomic) NSString *matchID;
@property (strong, nonatomic) NSString *matchName;
@property (strong, nonatomic) NSString *matchDate;
@property (assign, nonatomic) BOOL allowBet;
@property (strong, nonatomic) NSString *homeClub;
@property (strong, nonatomic) NSString *awayClub;
@property (assign, nonatomic) int bet_min;
@property (assign, nonatomic) int bet_max;
@end

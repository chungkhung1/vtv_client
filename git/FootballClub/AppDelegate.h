//
//  AppDelegate.h
//  FootballClub
//
//  Created by Luong The Dung on 12/24/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


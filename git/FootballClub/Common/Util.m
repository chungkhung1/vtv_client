//

//

#import "Util.h"

@implementation Util
+ (void)showAlert:(NSString *)message withTitle:(NSString *)title atController:(UIViewController *)controller{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:cancelAction];
    [controller presentViewController:alert animated:YES completion:nil];
    
}

+ (void)saveObjectWithEncode:(id)object forkey:(NSString *)key{
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    NSData *data =[NSKeyedArchiver archivedDataWithRootObject:object];
    [userDefault removeObjectForKey:key];
    [userDefault synchronize];
    [userDefault setObject:data forKey:key];
    [userDefault synchronize];
}

+(id)getObjectWithDecode:(NSString *)key{
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    NSData *data =[userDefault objectForKey:key];
    return (id)[NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+(void)callPhoneNumber:(NSString *)phone{
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    NSString *cleanedString = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    else{
    }
}

+ (void)setObject:(id)obj forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:obj forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)removeObjectForKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end

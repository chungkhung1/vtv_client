//

//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Util : NSObject
+ (void)showAlert:(NSString *)message withTitle:(NSString *)title atController:(UIViewController*)controller;
+ (void)saveObjectWithEncode:(id)object forkey:(NSString*)key;

+ (id)getObjectWithDecode:(NSString*)key;

+(void)callPhoneNumber:(NSString *)phone;

+ (void)setObject:(id)obj forKey:(NSString *)key;
+ (void)removeObjectForKey:(NSString *)key;

@end

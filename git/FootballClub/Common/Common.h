//
//  Common.h
//  FootballClub
//
//  Created by Luong The Dung on 12/24/16.
//  Copyright © 2016 Luong The Dung. All rights reserved.
//

#ifndef Common_h
#define Common_h
#define APP_NAME @"FootballClub"

#define BASE_DOMAIN @"http://quanbongda.net/api/v1/"
#define BASE_BET @"betWithUserLimit"


#define BET_MATCH @"betMatch"
#define UPDATEPROFILE @"user/updateProfile"
#define PREFIX_IMAGE_URL @"http://quanbongda.net"
#define kStatusBarStyle UIStatusBarStyleLightContent;

#define IMAGE_HOLDER [UIImage imageNamed:@"imgHolder.png"]
#define AVATAR_HOLDER [UIImage imageNamed:@"imgAvatarHolder.png"]

//------- Define Colors use in app -------//
#define COLOR_BG_NAVIBAR [UIColor \
colorWithRed:((float)((0x272e59 & 0xFF0000) >> 16))/255.0 \
green:((float)((0x272e59 & 0xFF00) >> 8))/255.0 \
blue:((float)(0x272e59 & 0xFF))/255.0 alpha:1.0]
#endif /* Common_h */

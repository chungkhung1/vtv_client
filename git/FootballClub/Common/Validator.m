

#import "Validator.h"

@implementation Validator
+ (NSString *)getSafeString:(id)obj {
    if (obj == nil || [obj isKindOfClass:[NSNull class]]) {
        return @"";
    }
    if (obj == NULL) {
        return @"";
    }
    if ([obj isKindOfClass:[NSString class]]) {
        return obj;
    }
    if ([obj isKindOfClass:[NSDictionary class]]) {
        return @"";
    }
    if ([obj isKindOfClass:[NSDate class]]) {
        return [NSString stringWithFormat:@"%@",obj];
    }
    return [obj stringValue];
}

+ (BOOL)validateEmail:(NSString*)email
{
    email = [email lowercaseString];
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [regExPredicate evaluateWithObject:email];
}

+ (float)getSafeFloat:(id)obj {
    
    if (obj == nil || [obj isKindOfClass:[NSNull class]]) {
        return 0.0;
    }
    if ([obj isKindOfClass:[NSNumber class]]) {
        return [obj floatValue];
    }
    if ([obj length] == 0) {
        return 0.0;
    }
    if ([obj isKindOfClass:[NSDictionary class]]) {
        return 0.0;
    }
    return [obj floatValue];
}
+ (NSInteger)getSafeInterger:(id)obj {
    
    if (obj == nil || [obj isKindOfClass:[NSNull class]]) {
        return 0.0;
    }
    if ([obj isKindOfClass:[NSNumber class]]) {
        return [obj floatValue];
    }
    if ([obj length] == 0) {
        return 0.0;
    }
    if ([obj isKindOfClass:[NSDictionary class]]) {
        return 0.0;
    }
    return [obj floatValue];
}

@end

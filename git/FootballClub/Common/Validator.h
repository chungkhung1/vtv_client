//

//

#import <Foundation/Foundation.h>

@interface Validator : NSObject
+ (NSString *)getSafeString:(id)obj;
+ (BOOL)validateEmail:(NSString*)email;
+ (float)getSafeFloat:(id)obj;
+ (NSInteger)getSafeInterger:(id)obj ;
@end
